<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Товар 1',
            'slug' => 'tovar-1',
            'short_description' => 'Обычный товар',
            'description' => 'Полное описание обычного товара',
            'category_id' => 1,
            'active' => 1,
            'price' => 1500,
            'income_price' => 1100,
        ]);

        DB::table('products')->insert([
            'name' => 'Товар 2',
            'slug' => 'tovar-2',
            'short_description' => 'Необычный товар',
            'description' => 'Полное описание необычного товара',
            'category_id' => 1,
            'active' => 1,
            'price' => 1600,
            'income_price' => 1200,
        ]);

        DB::table('products')->insert([
            'name' => 'Товар 3',
            'slug' => 'tovar-3',
            'short_description' => 'Это Товар 3',
            'description' => 'Полное описание товара 3',
            'category_id' => 2,
            'active' => 1,
            'price' => 1400,
            'income_price' => 900,
        ]);

        DB::table('products')->insert([
            'name' => 'Товар 4',
            'slug' => 'tovar-4',
            'short_description' => 'Это офигенный товар',
            'description' => ' Полное описание офигенного товара',
            'category_id' => 3,
            'active' => 1,
            'price' => 1350,
            'income_price' => 900,
        ]);
    }
}
