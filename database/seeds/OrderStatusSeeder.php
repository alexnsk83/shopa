<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert([
            'name_en' => 'new',
            'name_ru' => 'новый'
        ]);

        DB::table('order_statuses')->insert([
            'name_en' => 'process',
            'name_ru' => 'в работе'
        ]);

        DB::table('order_statuses')->insert([
            'name_en' => 'done',
            'name_ru' => 'выполнен'
        ]);
    }
}
