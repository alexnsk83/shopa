<?php

use Illuminate\Database\Seeder;

class DeliveriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deliveries')->insert([
            'name' => 'Самовывоз',
            'price' => 0,
            'pickup' => true,
            'pickup_address' => 'Адрес пункта выдачи',
        ]);

        DB::table('deliveries')->insert([
            'name' => 'курьерская доставка',
            'price' => 200,
            'pickup' => false
        ]);
    }
}
