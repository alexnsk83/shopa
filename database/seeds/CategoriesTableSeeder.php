<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Категория 1',
            'slug' => 'kategorija-1'
        ]);

        Category::create([
            'name' => 'Категория 2',
            'slug' => 'kategorija-2'
        ]);

        Category::create([
            'name' => 'Категория 3',
            'slug' => 'kategorija-3'
        ]);
    }
}
