<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', "MainController@index")->name('home');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', 'AuthController@register')->name('register');
    Route::post('/register', 'AuthController@registerPost')->name('registerPost');

    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/login', 'AuthController@loginPost')->name('Post');
});

Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('/shop/category/{slug}', 'ShopController@category')->name('category');
Route::get('/shop/category/{slug}/{subslug}', 'ShopController@category')->name('subcategory');
Route::get('/shop/product/{category}/{slug}', 'ShopController@product')->name('product');

Route::group(['prefix' => 'cart'], function () {
    Route::post('/add', 'ShopController@addToCart')->name('addToCart');
    Route::get('/remove/{id}', 'ShopController@removeProduct');
    Route::post('/change', 'ShopController@changeQuantity');
    Route::get('/', 'ShopController@showCart')->name('cart.show');
    Route::post('/', 'ShopController@submitOrder')->name('cart.submit');
});
