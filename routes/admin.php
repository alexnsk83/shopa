<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login',    'UserController@login')->       name('login');
    Route::post('/login',   'UserController@loginPost')->   name('login.post');
});

Route::group(['middleware' => 'isAdmin'], function () {
    Route::get('/logout',   'UserController@logout')->  name('logout');
    Route::get('/',         'HomeController@index')->   name('home');

    Route::group(['prefix' => 'category'], function () {
        Route::get ('',            'CategoryController@index')->   name('category.list');
        Route::get ('{id}',        'CategoryController@show')->    name('category.show')->     where('id', '[0-9]+');
        Route::post('',            'CategoryController@update')->  name('category.update');
        Route::get ('new',         'CategoryController@create')->  name('category.create');
        Route::post('new',         'CategoryController@store')->   name('category.store');
        Route::get ('delete/{id}', 'CategoryController@delete')->  name('category.delete')->   where('id', '[0-9]+');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('',            'ProductController@index')->   name('product.list');
        Route::get('{id}',        'ProductController@show')->    name('product.show')->     where('id', '[0-9]+');
        Route::post('',           'ProductController@update')->  name('product.update');
        Route::get('new',         'ProductController@create')->  name('product.create');
        Route::post('new',        'ProductController@store')->   name('product.store');
        Route::get('delete/{id}', 'ProductController@delete')->  name('product.delete')->   where('id', '[0-9]+');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get ('',       'UserController@index')->   name('user.list');
        Route::get ('{user}', 'UserController@show')->    name('user.show')->   where('user', '[0-9]+');
        Route::post('',       'UserController@update')->  name('user.update');
    });

    Route::group(['prefix' => 'delivery'], function () {
        Route::get ('',                  'DeliveryController@index')->  name('delivery.list');
        Route::get ('{delivery}',        'DeliveryController@show')->   name('delivery.show')->     where('delivery', '[0-9]+');
        Route::post('{delivery}',        'DeliveryController@update')-> name('delivery.update')->   where('delivery', '[0-9]+');
        Route::get ('new',               'DeliveryController@create')-> name('delivery.create');
        Route::post('new',               'DeliveryController@store')->  name('delivery.store');
        Route::get ('delete/{delivery}', 'DeliveryController@delete')-> name('delivery.delete')->   where('delivery', '[0-9]+');
    });

    Route::group(['prefix' => 'order'], function () {
        Route::get('', 'OrderController@index')         ->name('order.list');
        Route::get('{order}', 'OrderController@show')   ->name('order.show')->where('order', '[0-9]+');
    });

    Route::get('/ajax/image/delete/{id}', "ImageController@ajaxDelete");
});
