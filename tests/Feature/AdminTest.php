<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /** @test */
    public function notAuthorizedAccessToPanel()
    {
        $response = $this->get('/panel');
        $response->assertRedirect('/panel/login');
        $response->assertStatus(302);
    }

    /** @test */
    public function authorizedAccessToPanel()
    {
        $this->actingAs(
            factory(User::class)->create(['is_admin' => true])
        );
        $response = $this->get('/panel');
        $response->assertOk();
    }

    /** @test */
    public function getCategoriesList()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(
            factory(User::class)->create(['is_admin' => true])
        );
        $response = $this->get('/panel/category');
        $this->assertCount(1, $response->json());
    }
}
