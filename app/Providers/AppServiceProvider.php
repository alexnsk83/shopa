<?php

namespace App\Providers;

use App\Models\Cart;
use App\Models\Category;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Date::setLocale(config('app.locale'));
        $this->categoriesMenu();
        $this->cartState();
    }

    public function categoriesMenu()
    {
        View::composer('layouts.base', function ($view) {
            $view->with('categories', Category::where('parent_id', null)->get());
        });
    }

    public function cartState()
    {
        View::composer('layouts.base', function ($view) {
            $cart = new Cart();
            $view->with('cart', $cart->getCartState());
        });
    }
}
