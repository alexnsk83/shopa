<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\CrudInterface;
use App\Http\Requests\ProductRequest;
use App\Http\Traits\HelperTrait;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProductController extends Controller
{
    use HelperTrait;


    public function index() :View
    {
        $products = Product::orderBy('name')->paginate(20);

        return view('admin.product.list', compact('products'));
    }

    public function show($id) :View
    {
        $categories = Category::selectAllOrderedByName()->get();
        $product = Product::findOrFail($id);

        return view('admin.product.show', compact('categories', 'product'));
    }

    public function update(ProductRequest $request) :RedirectResponse
    {
        $product = Product::findOrFail($request->id);
        $product->category_id = $request->category_id == 0 ? null : $request->category_id;
        $product->update($request->input());
        if($request->file('main_image')) {
            $this->changeMainImage($request->file('main_image'), $product);
        }
        if ($request->file('images')) {
            $this->uploadImages($request->file('images'), $product);
        }

        return redirect()->route('admin.product.list');
    }

    public function create()
    {
        $categories = Category::orderBy('name')->get();

        return view('admin.product.new', compact('categories'));
    }


    public function store(ProductRequest $request)
    {
        $product = new Product();
        $product->category_id = $request->category_id == 0 ? null : $request->category_id;
        $product->fill($request->input());
        $product->save();
        if ($request->file('main_image')) {
            $this->uploadImages([$request->file('main_image')], $product, true);
        }
        if ($request->file('images')) {
            $this->uploadImages($request->file('images'), $product);
        }

        return redirect()->route('admin.product.list');
    }


    public function delete(int $id)
    {
        Product::destroy($id);

        return redirect()->back();
    }


    private function uploadImages($files, $model, $is_main = null) :void
    {
        foreach ($files as $file) {
            $filename = $this->translit($file->getClientOriginalName());
            $file->move(public_path() . '/images', $filename);
            if ($is_main) {
                $model->mainImage()->create(['filename' => $filename, 'is_main' => $is_main]);
            } else {
                $model->images()->create(['filename' => $filename, 'is_main' => $is_main]);
            }
        }
    }

    private function changeMainImage($file, $model) :void
    {
        $filename = $this->translit($file->getClientOriginalName());
        $file->move(public_path() . '/images', $filename);
        if ($model->mainImage) {
            $model->mainImage()->update(['filename' => $filename]);
        } else {
            $model->mainImage()->create(['filename' => $filename, 'is_main' => 1]);
        }
    }
}
