<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\CrudInterface;
use App\Http\Requests\CategoryRequest;
use App\Http\Traits\HelperTrait;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CategoryController extends Controller
{
    use HelperTrait;


    public function index() :View
    {
        $categories = Category::selectAllOrderedByParent()->paginate(20);

        return view('admin.category.list', compact('categories'));
    }


    public function create() :View
    {
        $categories = Category::selectAllOrderedByParent()->root()->get();

        return view('admin.category.new', compact('categories'));
    }


    public function store(CategoryRequest $request)
    {
        $category = new Category();
        $category->parent_id = $request->parent_id == 0 ? null : $request->parent_id;
        $category->fill($request->input());
        $category->save();
        if ($request->file()) {
            if ($category->image) {
                $category->image->delete();
            }
            $this->uploadImage($request->file('image'), $category);
        }

        return redirect()->route('admin.category.list');
    }


    public function show($id)
    {
        $category = Category::findOrFail($id);
        $others = Category::where('id', '!=', $id)->get();

        return view('admin.category.show', compact('category', 'others'));
    }


    public function update(CategoryRequest $request) :RedirectResponse
    {
        $category = Category::findOrFail($request->id);
        $category->parent_id = $request->parent_id == 0 ? null : $request->parent_id;
        $category->active = false;
        $category->update($request->input());

        if ($request->file()) {
            if ($category->image) {
                $category->image->delete();
            }
            $this->uploadImage($request->file('image'), $category);
        }

        return redirect()->route('admin.category.list');
    }


    public function delete($id) :RedirectResponse
    {
        Category::destroy($id);

        return redirect()->back();
    }


    private function uploadImage($file, $model) :void
    {
        $filename = $this->translit($file->getClientOriginalName());
        $file->move(public_path() . '/images', $filename);
        $model->image()->create(['filename' => $filename]);
    }
}
