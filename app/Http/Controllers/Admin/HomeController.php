<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->firstOrFail();
        $title = 'Главная';

        return view('admin.home', compact('user', 'title'));
    }
}
