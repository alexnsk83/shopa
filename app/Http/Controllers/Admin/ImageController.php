<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function ajaxDelete($id)
    {
        Image::destroy($id);
    }
}
