<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(20);

        return view('admin.user.list', compact('users'));
    }

    public function show(User $user)
    {
        $roles = Role::all();
        return view('admin.user.show', compact('user', 'roles'));
    }

    public function update(UserRequest $request)
    {
        $user = User::findOrFail($request->id);
        $user->update($request->input());

        return redirect()->route('admin.user.list');
    }

    public function login()
    {
        return view('admin.login');
    }

    public function loginPost(Request $request)
    {
        $authResult = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ], $request->remember);

        if ($authResult) {
            return redirect()->route('admin.home');
        } else {
            return redirect()->route('admin.login')->with('message', 'Неправильный логин или пароль');
        }
    }

    public function logout()
    {
        Auth::logout();

        return view('admin.login');
    }
}
