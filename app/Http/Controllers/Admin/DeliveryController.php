<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryRequest;
use App\Models\Delivery;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DeliveryController extends Controller
{
    public function index() :View
    {
        $deliveries = Delivery::paginate(20);

        return view('admin.delivery.list', compact('deliveries'));
    }

    public function show(Delivery $delivery)
    {
        return view('admin.delivery.show', compact('delivery'));
    }

    public function create()
    {
        return view('admin.delivery.new');
    }

    public function store(DeliveryRequest $request)
    {
        $delivery = new Delivery();
        $delivery->fill($request->all());
        $delivery->save();

        return redirect()->route('admin.delivery.list');
    }

    public function update(DeliveryRequest $request, Delivery $delivery)
    {
        $delivery->fill($request->all());
        $delivery->pickup = isset($request->pickup);
        $delivery->active = isset($request->active);
        $delivery->update();

        return redirect()->route('admin.delivery.list');
    }
}
