<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderCreateRequest;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Delivery;
use App\Models\Order;
use App\Models\OrderDelivery;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{
    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($slug, $subslug = null)
    {
        $category = Category::where('slug', $subslug ?? $slug)->first();

        return view('pages.catalog', compact('category'));
    }

    public function product($category, $slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();

        return view('pages.product', compact('product'));
    }

    /**
     * @param Request $request
     * @return void
     */
    public function addToCart(Request $request)
    {
        $cart = new Cart();
        $product = Product::find($request->product);
        $response = $cart->addProduct($request->product, $product->name, $request->price, $request->quantity);
        if($response) {
            $request->session()->put('cart', $cart);
        }

        echo json_encode($cart->getCartState(), JSON_UNESCAPED_UNICODE);
    }

    public function showCart()
    {
        $cart = Session::get('cart');
        $user = Auth::user() ? Auth::user() : new User();
        $deliveries = Delivery::active()->orderBy('pickup', 'desc')->get();

        return view('pages.cart', compact('cart', 'user', 'deliveries'));
    }

    public function submitOrder(OrderCreateRequest $request)
    {
        $order = new Order();
        $order->data = json_encode([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'comment' => $request->comment,
        ]);
        $order->user_id = Auth::id();
        $order->save();

        $total = 0;

        for ($i = 0; $i < count($request->product_id); $i++) {
            $product = Product::find($request->product_id[$i]);
            $order_item = new OrderItem();
            $order_item->order_id = $order->id;
            $order_item->product_id = $product->id;
            $order_item->quantity = $request->quantity[$i];
            $order_item->price = $product->price;
            $order_item->income_price = $product->income_price;
            $order_item->save();
            $total += $product->price * $request->quantity[$i];
        }
        $delivery = Delivery::find($request->delivery_id);
        $order_delivery = new OrderDelivery();
        $order_delivery->order_id = $order->id;
        $order_delivery->name = $delivery->name;
        $delivery_discount = (int)($total / 1000) * 50;
        $order_delivery->price = ($delivery->price - $delivery_discount) < 0 ? 0 : $delivery->price - $delivery_discount;
        $order_delivery->pickup = $delivery->pickup;
        $order_delivery->address = $delivery->pickup ? $delivery->address : $request->address;
        $order_delivery->save();

        $income_total = 0;
        $total = 0;
        foreach ($order->items as $item) {
            $income_total += $item->income_price * $item->quantity;
            $total += $item->price * $item->quantity;
        }
        $total += $order_delivery->price;
        $income_total += $order_delivery->price;
        $order->income_total = $income_total;
        $order->total = $total;
        $order->update();

        Cart::clearCart();

        return redirect()->route('home');
    }

    public function removeProduct($id)
    {
        $cart = Session::get('cart');
        $response = $cart->removeProduct($id);
        if ($response) {
            session()->put('cart', $cart);
        }

        echo json_encode($cart->getCartState(), JSON_UNESCAPED_UNICODE);
    }

    public function changeQuantity(Request $request)
    {
        $cart = Session::get('cart');
        $response = $cart->changeQuantity($request->id, $request->quantity);
        if ($response) {
            session()->put('cart', $cart);
        }

        echo json_encode($cart->getCartState(), JSON_UNESCAPED_UNICODE);
    }
}
