<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Mail\UserRegistered;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class AuthController extends Controller
{
    public function register()
    {
        return view('pages.registration', [
            'title' => 'Регистрация',
        ]);
    }

    public function registerPost(StoreUser $request)
    {
        $user = new User();

        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->save();

        Mail::to($request->email)
            ->send(new UserRegistered($request->name));

        return redirect()->route('login')->with('message', 'Вы успешно зарегистрировались, теперь можно авторизоваться!');
    }

    public function login()
    {
        return view('pages.login', [
            'title' => "Вход",
        ]);
    }

    public function loginPost(Request $request)
    {
        $remember = $request->input('remember') ? true : false;

        $authResult = Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ], $remember);

        if ($authResult) {
            return redirect()->route('home');
        } else {
            return redirect()->route('login')->with('authError', 'Неправильный логин или пароль');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->back();
    }
}
