<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $_POST['id'] ?? null; // Чтобы исключить собственный id из проверки на уникальность
        return [
            'name' => "required|min:2",
            'email' => "required|min:3|unique:users,email,$id|email",
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Необходимо указать имя",
            'name.min' => "Имя слишком короткое",
            'email.required' => "Адрес электронной почты обязателен",
            'email.unique' => "Такой адрес электронной почты уже существует",
            'email.min' => "Адрес электронной почты слишком короткий",
            'email.email' => "Адрес электронной почты указан некорректон",
        ];
    }
}
