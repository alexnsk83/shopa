<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $_POST['id'] ?? null; // Чтобы исключить собственный id из проверки на уникальность
        return [
            'name' => "required|min:3|unique:products,name,$id",
            'slug' => "required|min:3|unique:products,slug,$id",
            'main_image' => 'file|image',
            'images.*' => 'file|image',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Необходимо написать название",
            'name.unique' => "Такое название уже существует",
            'name.min' => "Название слишком короткое",
            'slug.required' => "Необходимо написать алиас",
            'slug.unique' => "Такой алиас уже существует",
            'slug.min' => "Алиас слишком короткий",
            'main_image.image' => "Главное изображение должно быть изображением",
            'images.*.image' => "Дополнительные изображения должны быть изображениями",
        ];
    }
}
