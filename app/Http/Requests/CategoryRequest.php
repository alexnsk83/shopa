<?php

namespace App\Http\Requests;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $_POST['id'] ?? null;
        return [
            'name' => "required|unique:categories,name,$id|min:3",
            'slug' => "required|unique:categories,slug,$id|min:3" ,
            'image' => 'file|image'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Необходимо написать название",
            'name.unique' => "Такое название уже существует",
            'name.min' => "Название слишком короткое",
            'slug.required' => "Необходимо написать алиас",
            'slug.unique' => "Такой алиас уже существует",
            'slug.min' => "Алиас слишком короткий",
            'image.image' => "Файл должен быть изображением"
        ];
    }
}
