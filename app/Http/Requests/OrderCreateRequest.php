<?php

namespace App\Http\Requests;

use App\Models\Delivery;
use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'delivery_id' => 'required',
            'address' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя не указано',
            'email.required' => 'E-mail не указан',
            'phone.required' => 'Телефон не указан',
            'delivery_id.required' => 'Способ доставки не выбран',
            'address.required' => 'Для доставки необходимо указать адрес'
        ];
    }

    protected function prepareForValidation()
    {
        if (isset($this->delivery_id) && !isset($this->address)) {
            $delivery = Delivery::find($this->delivery_id);
            if ($delivery->pickup) {
                $this->merge([
                    'address' => 'Самовывоз'
                ]);
            }
        }
        if (isset($this->name)) {
            $this->merge([
                'name' => ucwords(mb_strtolower($this->name))
            ]);
        }
        if (isset($this->email)) {
            $this->merge([
                'email' => mb_strtolower($this->email)
            ]);
        }
    }
}
