<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];


    public function model()
    {
        return $this->morphTo();
    }

    public function imageable()
    {
        return $this->morphTo(Image::class, 'imageable');
    }

    public static function isNotImage(&$file){
        return (self::getFileType($file) !== 'image');
    }

    protected static function getFileType(&$file){
        $finfo = finfo_open();

        $mime_type = finfo_buffer($finfo, $file, FILEINFO_MIME_TYPE);
        $splited_mime_type = explode( '/', $mime_type );
        $file_type = $splited_mime_type[0];

        finfo_close($finfo);

        return $file_type;
    }
}
