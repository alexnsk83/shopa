<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function delivery()
    {
        return $this->hasOne(OrderDelivery::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id', 'id');
    }

    public function getNumberAttribute()
    {
        return "№" . $this->attributes['id'] . ' от ' . Carbon::parse($this->attributes['created_at'])->format('d.m.Y');
    }

    public function getDataAttribute()
    {
        return json_decode($this->attributes['data']);
    }
}
