<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'price',
        'pickup',
        'pickup_address',
        'active'
    ];

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
