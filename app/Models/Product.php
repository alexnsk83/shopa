<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $guarded = ['category_id'];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function mainImage()
    {
        return $this->morphOne(Image::class, 'imageable')->where('is_main', 1);
    }


    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->where('is_main', null)->orWhere('is_main', 0);
    }

    public function allImages()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
