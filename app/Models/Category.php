<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $guarded = ['parent_id'];


    public function getProductsAttribute()
    {
        $categories = $this->children()->pluck('id')->toArray();
        $categories[] = $this->attributes['id'];
        $products = Product::whereIn('category_id', $categories)->get();

        return $products;
    }


    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }


    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }


    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->where('active', true);
    }


    public static function selectAllOrderedByName()
    {
        return Category::orderBy('name');
    }


    public static function selectAllOrderedByParent()
    {
        return Category::orderBy('parent_id');
    }


    public static function selectAllSortedByRelations()
    {
        return Category::orderBy('name')->root()->with('children');
    }


    public function scopeRoot($query)
    {
        return $query->where('parent_id', null);
    }
}
