<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Integer;

class Cart
{
    public $items = null;
    public $total_quantity = 0;
    public $total_price = 0;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        if (Session::has('cart'))
        {
            $old_cart = Session::get('cart');
            $this->items = $old_cart->items;
            $this->total_quantity = $old_cart->total_quantity;
            $this->total_price = $old_cart->total_price;
        }
    }

    /**
     * @return string
     */
    public function getCartState(): string
    {
        if($this->items) {
            $ending = $this->getWordEnding($this->total_quantity);
            return $this->total_quantity . ' товар' . $ending . ' на ' . $this->total_price;
        }

        return 'Корзина пуста';
    }

    /**
     * @param Request $request
     * @param Integer $id
     * @param Integer $price
     * @param Integer $quantity
     * @return bool
     */
    public function addProduct($id, $name, $price, $quantity): bool
    {
        $stored_item = ['id' => $id, 'name' => $name, 'price' => $price, 'quantity' => 0];
        if ($this->items) {
            if(array_key_exists($id, $this->items)) {
                $stored_item = $this->items[$id];
            }
        }
        $stored_item['name'] = $name;
        $stored_item['price'] = $price;
        $stored_item['quantity'] += $quantity;
        $this->items[$id] = $stored_item;
        $this->total_price += $price * $quantity;
        $this->total_quantity += $quantity;

        return true;
    }

    public function removeProduct($id)
    {
        $quantity = $this->items[$id]['quantity'];
        $sum = $this->items[$id]['price'] * $quantity;
        $this->total_quantity -= $quantity;
        $this->total_price -= $sum;
        unset($this->items[$id]);

        return true;
    }

    public function changeQuantity($id, $quantity)
    {
        $old_quantity = $this->items[$id]['quantity'];
        $old_subtotal = $this->items[$id]['price'] * $old_quantity;
        $this->items[$id]['quantity'] = $quantity;
        $this->total_quantity -= $old_quantity;
        $this->total_quantity += $quantity;
        $this->total_price -= $old_subtotal;
        $this->total_price += $this->items[$id]['price'] * $quantity;

        return true;
    }

    public static function clearCart()
    {
        Session::flush('cart');
    }

    /**
     * @param $number
     * @return string
     */
    private function getWordEnding($number): string
    {
        $ending = '';
        if ($number % 10 == 0)
            $ending = 'ов';
        if(in_array(($number % 10), [2, 3, 4]))
            $ending = 'а';
        if(($number % 10) > 4)
            $ending = 'ов';
        if(in_array($number, [11, 12, 13, 14]))
            $ending = 'ов';

        return $ending;
    }
}
