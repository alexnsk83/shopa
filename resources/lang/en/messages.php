<?php
return [
    'registered' => 'You successfully registered? now you may enter',
    'post_created' => 'Post created',
    'post_saved' => 'Post saved',
    'post_deleted' => 'Post deleted',
    'post_recovered' => 'Post recovered',
    'user_saved' => 'Profile changes saved',
    'user_deleted' => 'User deleted',
    'published' => 'Published',
    'page_created' => 'Page created',
    'page_updated' => 'Page updated',
    'page_deleted' => 'Page deleted',
    'gallery_created' => 'Gallery created',
    'gallery_updated' => 'Gallery updated',
    'gallery_deleted' => 'Gallery deleted',
    'gallery_recovered' => 'Gallery recovered',
];