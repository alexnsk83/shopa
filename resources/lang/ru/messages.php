<?php
return [
    'registered' => 'Вы успешно зарегистрировались, теперь можно авторизоваться!',
    'post_created' => 'Статья создана',
    'post_saved' => 'Статья сохранена',
    'post_deleted' => 'Статья удалена',
    'post_recovered' => 'Статья восстановлена',
    'user_saved' => 'Изменения профиля сохранены',
    'user_deleted' => 'Пользователь удалён',
    'published' => 'Опубликовано',
    'page_created' => 'Страница создана',
    'page_updated' => 'Страница обновлена',
    'page_deleted' => 'Страница удалена',
    'gallery_created' => 'Галерея создана',
    'gallery_updated' => 'Галерея обновлена',
    'gallery_deleted' => 'Галерея удалена',
    'gallery_recovered' => 'Галерея восстановлена',
];