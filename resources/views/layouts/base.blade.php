<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ URL::to('/images/base/logo_dummy.png') }}">
    <link rel="stylesheet" href="{{ URL::to('/css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/css/custom.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/css/owl.theme.default.min.css') }}">
    <link href="https://allfont.ru/allfont.css?fonts=epson1" rel="stylesheet" type="text/css" />
    @yield('style')
    <script src="{{ URL::to('/js/jquery.min.js') }}"></script>
    <script src="{{ URL::to('/js/maskedinput.min.js') }}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <title>{{ config('app.name') }}</title>
</head>
<body>
    <div id="app">
        @include('blocks.header')
        @yield('content')
    </div>
<footer>
    <script src="{{ URL::to('/js/app.js') }}"></script>
    <script src="{{ URL::to('/js/custom.js') }}"></script>
    @yield('script')
</footer>
</body>
</html>
