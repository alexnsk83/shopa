@extends('layouts.base')

@section('content')
<div class="container">
    <div class="row flex-center">
        <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4">
            <h1>Регистрация</h1>
            <form enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <div class="container">
                    <div class="row">
                        <input type="email" name="email" placeholder="{{ trans('site.form.email') }}" value="{{ old('email') }}" class="form-control col-10">
                        <span class="col-2 text-danger">*</span>
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->get('email')[0] }} </span>
                        @endif
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <input type="password" name="password" placeholder="{{ trans('site.form.password') }}" class="form-control col-10">
                        <span class="col-2 text-danger">*</span>
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->get('password')[0] }} </span>
                        @endif
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <input type="password" name="password2" placeholder="{{ trans('site.form.password_repeat') }}" class="form-control col-10">
                        <span class="col-2 text-danger">*</span>
                        @if ($errors->has('password2'))
                            <span class="text-danger">{{ $errors->get('password2')[0] }} </span>
                        @endif
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <input type="text" name="name" placeholder="{{ trans('site.form.name') }}" value="{{ old('name') }}" class="form-control col-10">
                        <span class="col-2 text-danger">*</span>
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->get('name')[0] }} </span>
                        @endif
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <input id="phone" type="text" name="phone" placeholder="{{ trans('site.form.phone') }}" value="{{ old('phone') }}" class="form-control col-10">
                        <span class="col-2 text-danger">*</span>
                        @if ($errors->has('phone'))
                            <span class="text-danger">{{ $errors->get('phone')[0] }} </span>
                        @endif
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <input type="text" name="address" placeholder="{{ trans('site.form.address') }}" value="{{ old('address') }}" class="form-control col-10">
                        @if ($errors->has('address'))
                            <span class="text-danger">{{ $errors->get('address')[0] }} </span>
                        @endif
                    </div>
                </div>
                <br>
                <input type="submit" name="reg" class="mt-1" value="{{ trans('site.button.register') }}"><br>
            </form>
        </div>
    </div>
</div>
    <script>
        $("#phone").mask("8(999) 999-9999");
    </script>
@endsection
