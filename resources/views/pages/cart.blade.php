@extends('layouts.base')

@section('content')
    <cart-component :cart="{{ json_encode($cart) }}"
                    :user="{{ json_encode($user) }}"
                    :route="{{ json_encode(route('cart.submit')) }}"
                    :deliveries="{{ json_encode($deliveries) }}"
                    :csrf="{{ json_encode(csrf_token()) }}"
                    :old="{{ json_encode(old()) }}"></cart-component>
@endsection

