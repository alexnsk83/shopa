@extends('layouts.base')

@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="slider d-none d-md-block w100">
            <div class="owl-carousel owl-theme">
                <div class="lifted">
                    <img src="{{ URL::to('/images/slider/dummy-640x310-1.jpg') }}">
                </div>
                <div class="lifted">
                    <img src="{{ URL::to('/images/slider/dummy-640x310-2.jpg') }}">
                </div>
                <div class="lifted">
                    <img src="{{ URL::to('/images/slider/dummy-640x310-3.jpg') }}">
                </div>
                <div class="lifted">
                    <img src="{{ URL::to('/images/slider/dummy-640x310-4.jpg') }}">
                </div>
            </div>
            <script type="application/javascript">
                $(window).on("load", function() {
                    $('.owl-carousel').owlCarousel({
                        loop:true, //Зацикливаем слайдер
                        margin:0, //Отступ от элемента справа в 50px
                        nav:false, //Отключение навигации
                        autoplay:true, //Автозапуск слайдера
                        smartSpeed:1000, //Время движения слайда
                        autoplayTimeout:6000, //Время смены слайда
                        autoplayHoverPause:true,
                        responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                            700:{items:1},
                            950:{items:2},
                            1200:{items:3},
                            1550:{items:3}
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ URL::to('/js/owl.carousel.min.js') }}"></script>
@endsection
