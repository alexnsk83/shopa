@extends('layouts.base')

@section('content')
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-12">
                <div class="card my-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-5">
                                <h1 class="d-inline-block d-sm-none">{{ $product->name }}</h1>
                                <div class="col-12">
                                    <img src="/images/{{ $product->mainImage->filename ?? 'base/nophoto.jpg' }}" class="product-image" alt="Product Image">
                                </div>
                                @if (count($product->images))
                                <div class="col-12 product-image-thumbs">
                                    @foreach($product->images as $image)
                                        <div class="product-image-thumb"><img src="/images/{{ $image->filename }}" alt="Product Image" width="100%"></div>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                            <div class="col-12 col-sm-6 col-md-7">
                                <h2 class="my-3">{{ $product->name }}</h2>
                                <p>{{ $product->short_description }}</p>

                                <hr>

                                <div class=" py-2 px-3 mt-4">
                                    <h3 class="mb-0">
                                        {{ $product->price }}р.
                                    </h3>
                                </div>

                                <div class="mt-4 buy_block">
                                    <input type="number" class="quantity">
                                    <div class="buy btn btn-primary" data-id="{{ $product->id }}" data-price="{{ $product->price }}" data-quantity="1">
                                        <i class="fas fa-cart-plus fa-lg mr-2"></i>
                                        Добавить в корзину
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-12">
                                        <nav class="w-100">
                                            <div class="nav nav-tabs" id="product-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">Описание</a>
                                                <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#product-comments" role="tab" aria-controls="product-comments" aria-selected="false">Комментарии</a>
                                                <a class="nav-item nav-link" id="product-rating-tab" data-toggle="tab" href="#product-rating" role="tab" aria-controls="product-rating" aria-selected="false">Rating</a>
                                            </div>
                                        </nav>
                                        <div class="tab-content p-3" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab">{{ $product->description }}</div>
                                            <div class="tab-pane fade" id="product-comments" role="tabpanel" aria-labelledby="product-comments-tab"> Vivamus rhoncus nisl sed venenatis luctus. Sed condimentum risus ut tortor feugiat laoreet. Suspendisse potenti. Donec et finibus sem, ut commodo lectus. Cras eget neque dignissim, placerat orci interdum, venenatis odio. Nulla turpis elit, consequat eu eros ac, consectetur fringilla urna. Duis gravida ex pulvinar mauris ornare, eget porttitor enim vulputate. Mauris hendrerit, massa nec aliquam cursus, ex elit euismod lorem, vehicula rhoncus nisl dui sit amet eros. Nulla turpis lorem, dignissim a sapien eget, ultrices venenatis dolor. Curabitur vel turpis at magna elementum hendrerit vel id dui. Curabitur a ex ullamcorper, ornare velit vel, tincidunt ipsum. </div>
                                            <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab"> Cras ut ipsum ornare, aliquam ipsum non, posuere elit. In hac habitasse platea dictumst. Aenean elementum leo augue, id fermentum risus efficitur vel. Nulla iaculis malesuada scelerisque. Praesent vel ipsum felis. Ut molestie, purus aliquam placerat sollicitudin, mi ligula euismod neque, non bibendum nibh neque et erat. Etiam dignissim aliquam ligula, aliquet feugiat nibh rhoncus ut. Aliquam efficitur lacinia lacinia. Morbi ac molestie lectus, vitae hendrerit nisl. Nullam metus odio, malesuada in vehicula at, consectetur nec justo. Quisque suscipit odio velit, at accumsan urna vestibulum a. Proin dictum, urna ut varius consectetur, sapien justo porta lectus, at mollis nisi orci et nulla. Donec pellentesque tortor vel nisl commodo ullamcorper. Donec varius massa at semper posuere. Integer finibus orci vitae vehicula placerat. </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ URL::to('/css/number_plugin.css') }}">
@endsection

@section('script')
    <script src="/js/number_plugin.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.quantity').number_plugin({
                min: 1,
                height: '37px',
                width: '50px',
                animate: true
            });

            $('.buy').click(function (e) {
                e.preventDefault();
                $(this).prop('disabled', true);
                var product = $(this).data('id'),
                    price = $(this).data('price'),
                    quantity = $(this).parent('.buy_block').find('.quantity').val();
                if (quantity < 0) {
                    quantity = -1 * quantity;
                }
                var that = $(this);
                $.ajax({
                    type: "POST",
                    url: "/cart/add",
                    data: {product: product, price: price, quantity:quantity},
                    success: function (response) {
                        $('#shopping_cart').text(JSON.parse(response));
                        that.prop('disabled', false);
                    }
                });
            });
        });
    </script>
@endsection
