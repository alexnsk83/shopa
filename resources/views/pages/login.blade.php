@extends('layouts.base')

@section('content')
<div class="container">
    <div class="row flex-center">
        <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4">
                <h1>Авторизация</h1>
                @if(Session::has('message'))
                    {{Session::get('message')}}
                @endif
                <form method="post">
                    {{ csrf_field() }}
                    <input type="text" class="form-control" name="email" placeholder="{{ trans('site.form.email') }}" value="{{ old('email') }}"><br>
                    <input type="password" class="form-control" name="password" placeholder="{{ trans('site.form.password') }}"><br>
                    <input type="checkbox" id="remember" name="remember"><label for="remember"><span>{{ trans('site.form.remember') }}</span></label><br>
                    <input type="submit" name="auth" value="{{ trans('site.button.login') }}"><br>
                    <a href="{{ route('register') }}">{{ trans('site.button.register') }}</a>
                </form>
                @if (session('authError'))
                    <p class="text-danger">{{ session('authError') }}</p>
                @endif
        </div>
    </div>
</div>
@endsection
