@extends('layouts.base')

@section('content')
    <div class="container-fluid">
        <h3 class="category-title">{{ $category->name }}</h3>
        @if (count($category->children))
            <div class="row">
                <div class="col-12">
                    <div class="card subcategories">
                        <div class="card-body">
                            <div class="row">
                                @foreach($category->children as $subcategory)
                                    <div class="col-1">
                                        <a href="{{ route('subcategory', ['slug' => $category->slug, 'subslug' => $subcategory->slug]) }}">
                                            <div class="card subcat-card">
                                                <div class="card-body">
                                                    <div class="image">
                                                        <img src="/images/{{ $subcategory->image->filename ?? 'base/nophoto.jpg' }}" alt="" class="subcat-image">
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    {{ $subcategory->name }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row flex-center">
            @foreach($category->products as $product)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="product-card card my-2">
                        <div class="card-body">
                            <a  href="{{ route('product', ['category' => $category->slug, 'slug' => $product->slug]) }}" class="image mb-1">
                                <img src="/images/{{ $product->mainImage->filename ?? 'base/nophoto.jpg' }}" alt="" class="card-img-top">
                            </a>
                            <a class="product-link" href="{{ route('product', ['category' => $category->slug, 'slug' => $product->slug]) }}">
                                <div class="card-title">
                                        <h5>{{ $product->name }}</h5>
                                </div>
                                <p class="card-text product-short-description">{{ $product->short_description }}</p>
                                <p class="card-text">{{ $product->price }}р.</p>
                            </a>
                            <div class="buy_block">
                                <input type="number" class="quantity">
                                <div class="buy btn btn-outline-secondary" data-id="{{ $product->id }}" data-price="{{ $product->price }}" data-quantity="1">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Добавить в корзину
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              @endforeach
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ URL::to('/css/number_plugin.css') }}">
    <style>
        .product-link {
            color: #000 !important;
            text-decoration: none;
        }
        .product-link :hover {
            color: #000;
            text-decoration: none;
        }
        .product-card {
            width: 250px;
            box-shadow: 1px 1px 2px grey;
            margin: auto;
        }
        .product-short-description {
            height: 50px;
        }
        .product-card .buy_block {
            display: flex;
            justify-content: space-between;
            white-space: nowrap;
        }
        .subcategories {
            background-color: rgba(255, 255, 255, .5);
        }
        .subcat-card {
            width: 150px;
            box-shadow: 1px 1px 2px grey;
            margin: auto;
        }
        .subcat-image {
            height: 135px;
        }
        .subcat-card .image {
            overflow: hidden;
            display: flex;
            justify-content: space-around;
        }
        .subcat-card .image img {
            margin-left: auto;
            margin-right: auto;
            transition: all ease-in-out 1.0s;
            transform-origin: center center
        }
        .subcat-card .image img:hover {
            transition: all ease-in-out 2.5s;
            transform: scale(1.1);
        }
        .subcategories .card-footer {
            padding: 0.1em 0.2em;
        }
    </style>
@endsection

@section('script')
    <script src="/js/number_plugin.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.quantity').number_plugin({
                min: 1,
                height: '37px',
                width: '45px',
                animate: true
            });

            $('.buy').click(function (e) {
                $(this).prop('disabled', true);
                e.preventDefault();
                var product = $(this).data('id'),
                    price = $(this).data('price'),
                    quantity = $(this).parent('.buy_block').find('.quantity').val();
                if (quantity < 0) {
                    quantity = -1 * quantity;
                }
                var that = $(this);
                $.ajax({
                    type: "POST",
                    url: "/cart/add",
                    data: {product: product, price: price, quantity:quantity},
                    success: function (response) {
                        $('#shopping_cart').text(JSON.parse(response));
                        that.prop('disabled', false);
                    }
                });
            });
        });
    </script>
@endsection
