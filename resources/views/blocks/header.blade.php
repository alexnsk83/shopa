<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 col-md-3">
                <img src="{{ URL::to('/images/base/kudesnitsa_logo.jpg') }}" height="100">
            </div>
            <div class="d-none d-sm-none d-md-block col-md-4 col-lg-5 col-xl-6"></div>
            <div class="col-6 col-md-5 col-lg-4 col-xl-3">
                <div class="container mt-1">
                    <div class="row">
                        <div class="col-12 mt-2">
                            <div class="dropdown">
                                <a class="_dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{ URL::to('/images/base/user_avatar_empty.png') }}" alt="" class="user-avatar">
                                    {{ Auth::check() ? Auth::user()->name : 'Гость' }}
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        @if (Auth::check())
                                            <a class="dropdown-item" href="#">Личный кабинет</a>
                                            @if (Auth::user()->role->name == 'admin')
                                                <a class="dropdown-item" href="{{ route('admin.home') }}">Панель управления</a>
                                            @endif
                                            <a class="dropdown-item" href="{{ route('logout') }}">Выйти</a>
                                        @else
                                            <a class="dropdown-item" href="{{ route('login') }}">Войти</a>
                                            <a class="dropdown-item" href="{{ route('register') }}">Зарегистрироваться</a>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <a href="{{ route('cart.show') }}" class="d-flex">
                                <i class="fas fa-shopping-cart fa-2x"></i>
                                <span id="shopping_cart" class="ml-1">{{ $cart }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbar1">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">Главная</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Галерея</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Каталог</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                    @foreach($categories as $category)
                                        <a class="dropdown-item" href="{{ route('category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                                    @endforeach
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Новости</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Контакты</a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" aria-label="Search">
                            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Поиск</button>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
@if ($errors->any())
    <div class="alert alert-warning alert-dismissible fade show" role="alert" style="position: absolute; z-index: 10000; right: 50%; top: 7%; border: 1px solid red; padding: 1.75rem 2.25rem; padding-right: 5rem">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
