@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-gradient-primary">
                        <h3 class="card-title mt-2">Заказы</h3>
                        <div class="card-tools">
                            {{--<a href="{{ route('admin.product.create') }}" class="btn btn-default">Новый товар</a>--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <th>Номер</th>
                            <th>Статус</th>
                            <th>Заказчик</th>
                            <th>Себестоимость</th>
                            <th>Сумма</th>
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->number }}</td>
                                    <td>{{ $order->status->name_ru }}</td>
                                    <td>{{ $order->data->name }}</td>
                                    <td>{{ $order->income_total }}</td>
                                    <td>{{ $order->total }}</td>
                                    <td>
                                        <a href="{{ route('admin.order.show', $order->id) }}"><i class="fas fa-edit"></i></a>
                                        <a href="{{ route('admin.product.delete', $order->id) }}"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <ul class="pagination pagination-sm m-0 float-right">
                            {{ $orders->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
