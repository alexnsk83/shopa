
@extends('admin.layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-gradient-primary">
                    <h3 class="card-title mt-2">Заказ {{ $order->number }}</h3>
                    <div class="card-tools">
                        {{--<a href="{{ route('admin.product.create') }}" class="btn btn-default">Новый товар</a>--}}
                    </div>
                </div>
                <div class="card-body">
                    <h3>Состав заказа</h3>
                    <table class="table">
                        <thead>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>количество</th>
                        <th>Цена</th>
                        <th>Сумма</th>
                        </thead>
                        <tbody>
                        @php $i = 1 @endphp
                        @foreach($order->items as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $item->product->name }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td title="Входная цена: {{ $item->income_price }}">{{ $item->price }}</td>
                            <td title="Входная сумма: {{ $item->income_price * $item->quantity }}. Доход: {{ $item->price * $item->quantity - $item->income_price * $item->quantity }}">{{ $item->price * $item->quantity }}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td>{{ $order->delivery->name }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{ $order->delivery->price }}</td>
                        </tr>
                        <tr>
                            <td>Итого:</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td title="Себестоимость: {{ $order->income_total }}. Доход: {{ $order->total - $order->delivery->price - $order->income_total }}">{{ $order->total }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <h3>Данные покупателя</h3>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Имя:</td>
                            <td>{{ $order->data->name }}</td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>{{ $order->data->email }}</td>
                        </tr>
                        <tr>
                            <td>Телефон:</td>
                            <td>{{ $order->data->phone }}</td>
                        </tr>
                        <tr>
                            <td>Адрес:</td>
                            <td>{{ $order->data->address }}</td>
                        </tr>
                        <tr>
                            <td>Комментарий:</td>
                            <td>{{ $order->data->comment }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <ul class="pagination pagination-sm m-0 float-right">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
