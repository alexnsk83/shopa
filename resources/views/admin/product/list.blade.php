@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-gradient-primary">
                        <h3 class="card-title mt-2">Товары</h3>
                        <div class="card-tools">
                            <a href="{{ route('admin.product.create') }}" class="btn btn-default">Новый товар</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>Название</th>
                                <th>Категория</th>
                                <th>Цена</th>
                                <th>Себестоимость</th>
                                <th>Активно</th>
                                <th>Действия</th>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category->name ?? "Нет" }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->income_price }}</td>
                                        <td>{{ $product->active ? "Да" : "Нет" }}</td>
                                        <td>
                                            <a href="{{ route('admin.product.show', $product->id) }}"><i class="fas fa-edit"></i></a>
                                            <a href="{{ route('admin.product.delete', $product->id) }}"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <ul class="pagination pagination-sm m-0 float-right">
                            {{ $products->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
