@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.product.update') }}" method="post" enctype="multipart/form-data" role="form">
                    <div class="card">
                        <div class="card-header bg-gradient-success">
                            <h3 class="card-title">{{ $product->name }}</h3>
                        </div>
                        <div class="card-body">
                            @csrf
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <label>Название</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $product->name }}">
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label>Алиас</label>
                                        <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug') ?? $product->slug }}">
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>Категория</label>
                                        <select class="form-control" name="category_id">
                                            <option value="0">Нет</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{$category->id === $product->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">
                                        <label>Описание</label>
                                        <textarea class="form-control" name="description">{{ old('description') ?? $product->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Цена</label>
                                        <input type="text" class="form-control" id="name" name="price" value="{{ old('price') ?? $product->price }}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Входная цена</label>
                                        <input type="text" class="form-control" id="slug" name="income_price" value="{{ old('income_price') ?? $product->income_price }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 88px;">
                                            <div class="bootstrap-switch-container bootstrap-switch-cyan" style="width: 129px; margin-left: 0px;">
                                                <input type="checkbox" id="active" name="active" value="1" data-on-color="success" {{ $product->active ? 'checked' : '' }} data-bootstrap-switch>
                                            </div>
                                        </div>
                                        <label class="ml-1" for="active"> Активен</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Главное изображение</label>
                                <div class="row">
                                    @if ($product->mainImage)
                                        <div class="col-3">
                                            <a href="{{ config('app.images_path') . $product->mainImage->filename }}" class="image-shadow" data-toggle="lightbox" data-title="{{ $product->name }}" data-gallery="gallery">
                                                <img src="{{  config('app.images_path') . $product->mainImage->filename }}" class="img-fluid mb-2" alt="{{ $product->name }}">
                                            </a>
                                            <a href="#" data-id="{{ $product->mainImage->id }}" class="delete_image"><i class="fas fa-trash"></i></a>
                                        </div>
                                    @endif
                                    <div class="col-12 mt-2">
                                        <div class="custom-file">
                                            <input type="file" name="main_image" class="custom-file-input" id="mainFile">
                                            <label class="custom-file-label" for="mainFile">Главное изображение</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Изображения</label>
                                <div class="row">
                                    @if ($product->images)
                                        @foreach($product->images as $image)
                                            <div class="col-2">
                                                <a href="{{ config('app.images_path') . $image->filename }}" class="image-shadow" data-toggle="lightbox" data-title="{{ $product->name }}" data-gallery="gallery">
                                                    <img src="{{  config('app.images_path') . $image->filename }}" class="img-fluid mb-2" alt="{{ $product->name }}">
                                                </a>
                                                <a href="#" data-id="{{ $image->id }}" class="delete_image"><i class="fas fa-trash"></i></a>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="col-12 mt-2">
                                        <div class="custom-file">
                                            <input type="file" name="images[]" class="custom-file-input" id="otherFiles" multiple>
                                            <label class="custom-file-label" for="otherFiles">Изображение</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-success" value="Сохранить">
                            <a href="{{ route('admin.category.list') }}" class="btn btn-secondary">Отмена</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.product.script')
    <script>
        //Удаление картинок
        $('body').on('click', '.delete_image', function (e) {
            e.preventDefault();
            var conf = confirm("Точно удалить?");
            if (conf) {
                var image_id = $(this).data('id');
                console.log(image_id);
                $(this).parent().remove();
                $.get({
                    url: "/panel/ajax/image/delete/" + image_id,
                });
            }
        });
    </script>
@endsection
