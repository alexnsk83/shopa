<script src="/admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script>
    //заполнение поля Slug
    $('#name').keyup(function(){
        translit();
        return false;
    });

    $('#name').blur(function(){
        translit();
        return false;
    });

    function translit(){
        // Символ, на который будут заменяться все спецсимволы
        var space = '-';
        // Берем значение из нужного поля и переводим в нижний регистр
        var text = $('#name').val().toLowerCase();
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i',
            'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
            'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': space, 'ы': 'y',
            'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',

            ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space, '#': space, '$': space,
            '%': space, '^': space, '&': space, '*': space, '(': space, ')': space, '-': space, '\=': space,
            '+': space, '[': space, ']': space, '\\': space, '|': space, '/': space, '.': space, ',': space,
            '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space, '?': space, '<': space,
            '>': space, '№': space
        };

        var result = '';
        var current_symbol = '';

        for(i=0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if(transl[text[i]] != undefined) {
                if(current_symbol != transl[text[i]] || current_symbol != space){
                    result += transl[text[i]];
                    current_symbol = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                current_symbol = text[i];
            }
        }
        result = TrimStr(result);

        // Выводим результат
        $('#slug').val(result);
    }

    function TrimStr(s) {
        s = s.replace(/^-/, '');
        return s.replace(/-$/, '');
    }

    //Bootstrap-switch
    $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

    //bsCustomFileInput
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
