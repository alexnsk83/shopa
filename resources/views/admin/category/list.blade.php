@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-gradient-primary">
                        <h3 class="card-title mt-2">Категории</h3>
                        <div class="card-tools">
                            <a href="{{ route('admin.category.create') }}" class="btn btn-default">Новая категория</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>Название</th>
                                <th>Ссылка</th>
                                <th>Родитель</th>
                                <th>Активность</th>
                                <th>Действия</th>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->slug }}</td>
                                        <td>{{ $category->parent->name ?? "Нет" }}</td>
                                        <td>{{ $category->active ? "Да" : "Нет" }}</td>
                                        <td>
                                            <a href="{{ route('admin.category.show', $category->id) }}"><i class="fas fa-edit"></i></a>
                                            <a href="{{ route('admin.category.delete', $category->id) }}"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <ul class="pagination pagination-sm m-0 float-right">
                            {{ $categories->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
