@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.category.store') }}" method="post" enctype="multipart/form-data" role="form">
                    <div class="card">
                        <div class="card-header bg-gradient-cyan">
                            <h3 class="card-title">Новая категория</h3>
                        </div>
                        <div class="card-body">
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <label>Название</label>
                                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <label>Алиас</label>
                                            <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug') }}">
                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <label>Родительская категория</label>
                                            <select class="form-control" name="parent_id">
                                                <option value="0">Нет</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <label>Описание</label>
                                            <textarea class="form-control" name="description">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 88px;">
                                                <div class="bootstrap-switch-container bootstrap-switch-cyan" style="width: 129px; margin-left: 0px;">
                                                    <input type="checkbox" id="active" name="active" value="1" data-on-color="info" checked data-bootstrap-switch>
                                                </div>
                                            </div>
                                            <label class="ml-1" for="active"> Активна</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <label>Изображение</label>
                                            <div class="custom-file">
                                                <input type="file" name="image" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Изображение</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-info" value="Сохранить">
                            <a href="{{ route('admin.category.list') }}" class="btn btn-secondary">Отмена</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.category.script')
@endsection
