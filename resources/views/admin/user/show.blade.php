@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.user.update') }}" method="post" enctype="multipart/form-data" role="form">
                    <div class="card">
                        <div class="card-header bg-gradient-success">
                            <h3 class="card-title">Пользователь {{ $user->name }}</h3>
                        </div>
                        <div class="card-body">
                            @csrf
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <label>Имя</label>
                                        <input type="text" class="form-control" name="name" value="{{ old('name') ?? $user->name }}">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label>Роль</label>
                                        <select class="form-control" name="role_id">
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}" {{$role->id === $user->role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <label>E-mail</label>
                                        <input type="text" class="form-control" name="email" value="{{ old('email') ?? $user->email }}">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label>Телефон</label>
                                        <input type="text" class="form-control" name="phone" value="{{ old('phone') ?? $user->phone }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p>Количество заказов: {{ $user->orders->count() }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-success" value="Сохранить">
                            <a href="{{ route('admin.user.list') }}" class="btn btn-secondary">Отмена</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.category.script')
@endsection
