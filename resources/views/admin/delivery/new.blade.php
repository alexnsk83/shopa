@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.delivery.store') }}" method="post" enctype="multipart/form-data" role="form">
                    <div class="card">
                        <div class="card-header bg-gradient-cyan">
                            <h3 class="card-title">Новый способ доставки</h3>
                        </div>
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Название</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Стоимость</label>
                                        <input type="number" class="form-control" id="price" name="price" min="0" value="{{ old('price') ?? 0 }}">
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-sm-12">
                                        <label>Адрес пункта выдачи (если самовывоз)</label>
                                        <textarea class="form-control" name="pickup_address">{{ old('pickup_address') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 88px;">
                                            <div class="bootstrap-switch-container bootstrap-switch-cyan" style="width: 129px; margin-left: 0px;">
                                                <input type="checkbox" id="pickup" name="pickup" value="1" data-on-color="info" checked data-bootstrap-switch>
                                            </div>
                                        </div>
                                        <label class="ml-1" for="pickup"> Самовывоз</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 88px;">
                                            <div class="bootstrap-switch-container bootstrap-switch-cyan" style="width: 129px; margin-left: 0px;">
                                                <input type="checkbox" id="active" name="active" value="1" data-on-color="info" checked data-bootstrap-switch>
                                            </div>
                                        </div>
                                        <label class="ml-1" for="active"> Активна</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-info" value="Сохранить">
                            <a href="{{ route('admin.category.list') }}" class="btn btn-secondary">Отмена</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.category.script')
@endsection
