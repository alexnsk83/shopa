@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-gradient-primary">
                        <h3 class="card-title mt-2">Доставка</h3>
                        <div class="card-tools">
                            <a href="{{ route('admin.delivery.create') }}" class="btn btn-default">Новый способ доставки</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>Название</th>
                                <th>Стоимость</th>
                                <th>Самовывоз</th>
                                <th>Адрес самовывоза</th>
                                <th>Активен</th>
                                <th>Действия</th>
                            </thead>
                            <tbody>
                                @foreach($deliveries as $delivery)
                                    <tr>
                                        <td>{{ $delivery->name }}</td>
                                        <td>{{ $delivery->price }}</td>
                                        <td>{{ $delivery->pickup ? "Да" : "Нет" }}</td>
                                        <td>{{ $delivery->pickup ? $delivery->pickup_address : '' }}</td>
                                        <td>{{ $delivery->active ? "Да" : "Нет" }}</td>
                                        <td>
                                            <a href="{{ route('admin.delivery.show', $delivery->id) }}"><i class="fas fa-edit"></i></a>
                                            <a href="{{ route('admin.delivery.delete', $delivery->id) }}"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <ul class="pagination pagination-sm m-0 float-right">
                            {{ $deliveries->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
