@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.delivery.update', $delivery) }}" method="post" enctype="multipart/form-data" role="form">
                    <div class="card">
                        <div class="card-header bg-gradient-success">
                            <h3 class="card-title">{{ $delivery->name }}</h3>
                        </div>
                        <div class="card-body">
                            @csrf
                            <input type="hidden" name="id" value="{{ $delivery->id }}">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Название</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $delivery->name }}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Стоимость</label>
                                        <input type="number" class="form-control" id="price" name="price" min="0" value="{{ old('number') ?? $delivery->price }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="checkbox" name="pickup" data-bootstrap-switch data-off-color="danger" data-on-color="success"
                                            {{ count(old()) == 0 ? ($delivery->pickup ? 'checked' : '') : (old('pickup') ? 'checked' : '') }}>
                                        <label class="ml-1" for="pickup"> Самовывоз</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="checkbox" name="active" data-bootstrap-switch data-off-color="danger" data-on-color="success"
                                            {{ !count(old()) ? ($delivery->active ? 'checked' : '') : (old('active') ? 'checked' : '') }}>
                                        <label class="ml-1" for="active"> Активна</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-success" value="Сохранить">
                            <a href="{{ route('admin.delivery.list') }}" class="btn btn-secondary">Отмена</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.category.script')
@endsection
